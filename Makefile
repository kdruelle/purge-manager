################################################################################
#
# This file is part of purge-manager.
#
# (C) 2011 Kevin Druelle <kevin@druelle.info>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software.  If not, see <http://www.gnu.org/licenses/>.
# 
################################################################################

BIN_NAME = purge-manager
VERSION  = $(shell git describe --tags)
BUILDTIME= $(shell LANG=en_US.UTF-8; date +'%d %b %Y')

TARGETS="linux/amd64,linux/386,darwin/amd64,darwin/386,windows/amd64,windows/386"

DIST_DIR = dist
BIN_DIR  = bin

SHELL := /bin/bash

.PHONY: all xgo
all: xgo

xgo:
	@mkdir -p $(BIN_DIR) | true
	xgo -targets ${TARGETS} -ldflags '-s -w -X "main.version=${VERSION}" -X "main.buildTime=${BUILDTIME}"' -dest ./${BIN_DIR}/ -out ${BIN_NAME}-${VERSION} ./ #> /dev/null

zip: xgo
	@mkdir -p dist | true
	@echo create archives
	@find ./${BIN_DIR}/ -type f -regex '.*[^zip|gz]' -not -name "*windows*" -exec sh -c "basename {} | sed -E 's#(.*)#./${DIST_DIR}/\1.tar.gz \1#g' | xargs tar -C ./${BIN_DIR} -zcf" \;
	@find ./${BIN_DIR}/ -type f -regex '.*[^zip|gz]' -name "*windows*" -exec sh -c "basename {} | sed -E 's/.exe//g ; s#(.*)#./${DIST_DIR}/\1.zip ./${BIN_DIR}/\1.exe#g' | xargs zip" \;




local:
	go build -ldflags '-X "main.version=${VERSION}" -X "main.buildTime=${BUILDTIME}"'

linux:
	export GOPATH="$(shell go env GOPATH)"; \
	xgo -targets linux/amd64 -out ${BIN_NAME} \
	-ldflags "-X main.version=${VERSION} -X main.buildTime=${BUILDTIME}" \
	-dest ./bin/ ./

