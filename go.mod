module gorp

go 1.12

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/google/go-cmp v0.5.4 // indirect
	github.com/gorhill/cronexpr v0.0.0-20140423231348-a557574d6c02
	github.com/hashicorp/hcl v0.0.0-20180404174102-ef8a98b0bbce
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jmoiron/sqlx v1.3.1 // indirect
	github.com/juju/testing v0.0.0-20210302031854-2c7ee8570c07 // indirect
	github.com/kardianos/osext v0.0.0-20170510131534-ae77be60afb1 // indirect
	github.com/mitchellh/go-ps v0.0.0-20170309133038-4fdf99ab2936
	github.com/onsi/ginkgo v1.15.1 // indirect
	github.com/onsi/gomega v1.11.0 // indirect
	github.com/pingcap/check v0.0.0-20200212061837-5e12011dc712 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/sevlyar/go-daemon v0.1.3
	github.com/siddontang/go v0.0.0-20180604090527-bdc77568d726 // indirect
	github.com/siddontang/go-mysql v0.0.0-20180615023810-9fd7e448643e
	github.com/sirupsen/logrus v1.0.5
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.1 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83 // indirect
	golang.org/x/sys v0.0.0-20210309074719-68d13333faf2 // indirect
	golang.org/x/term v0.0.0-20210220032956-6a3ed077a48d // indirect
	golang.org/x/text v0.3.5 // indirect
	golang.org/x/tools v0.1.1-0.20210222172741-77e031214674 // indirect
	gopkg.in/airbrake/gobrake.v2 v2.0.9 // indirect
	gopkg.in/gemnasium/logrus-airbrake-hook.v2 v2.1.2 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	honnef.co/go/tools v0.1.1 // indirect
)
